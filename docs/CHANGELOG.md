## [0.5.11](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.5.10...v0.5.11) (2024-05-29)


### Bug Fixes

* fix the remaining incorrect hashes ([abfa0e4](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/abfa0e476619197a24fd2a4bdd0bbfe528b7cf31))

## [0.5.10](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.5.9...v0.5.10) (2024-05-28)


### Bug Fixes

* replace incorrect hashes for Nexus bundler combined integration test ([dc717d9](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/dc717d92b1fa9bac069dee8e4e02e73d5d805c11))

## [0.5.9](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.5.8...v0.5.9) (2024-02-12)


### Bug Fixes

* updated hoppr semver check ([03fb804](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/03fb80430ac186f5f039b796436acdf78d2b1167))

## [0.5.8](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.5.7...v0.5.8) (2024-01-28)


### Bug Fixes

* **deps:** update dependency hoppr to v1.11.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ea25801](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/ea25801c6b9290e81668299882a9abf9ff5c8f17))

## [0.5.7](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.5.6...v0.5.7) (2023-12-13)


### Bug Fixes

* **deps:** update dependency hoppr to v1.11.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([03c9b20](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/03c9b2003b7f6d70fe90268cbcdcfe9218d0e3a7))

## [0.5.6](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.5.5...v0.5.6) (2023-12-12)


### Bug Fixes

* **deps:** update dependency hoppr to v1.11.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([0a12271](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/0a12271e52779d2aede087ea63b34f0c1fa245fa))

## [0.5.5](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.5.4...v0.5.5) (2023-12-06)


### Bug Fixes

* **deps:** update dependency hoppr to v1.11.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([6e38b37](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/6e38b37f701c3e7d2ef5fd4ea6051e766ee1c94b))

## [0.5.4](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.5.3...v0.5.4) (2023-11-06)


### Bug Fixes

* **deps:** update dependency hoppr to v1.10.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([5d6b530](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/5d6b53005283bb9a4cc3b25b068740e263e7311f))

## [0.5.3](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.5.2...v0.5.3) (2023-08-24)


### Bug Fixes

* integration test installed hoppr version ([bbd616b](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/bbd616b4c931867d73d937e98084f67c22c8cefd))
* remove calls to `HopprPlugin.create_logger()` ([fceade7](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/fceade795ec20cc12c0005d602cfa27fe6d566a0))
* test using `ThreadPoolExecutor` ([dd75be2](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/dd75be2a43c52b4be5a678b37e0e64b8d44e9255))
* update logger import ([38c0867](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/38c086777773c4f6a7019bd4875b9a60096d44b5))

## [0.5.2](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.5.1...v0.5.2) (2023-08-01)


### Bug Fixes

* **deps:** update dependency hoppr to v1.9.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([38fefb9](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/38fefb9ed80524f0fda7cb18732da09ff3176d85))

## [0.5.1](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.5.0...v0.5.1) (2023-07-26)


### Bug Fixes

* **deps:** update dependency hoppr to v1.9.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([b28c311](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/b28c311a342163206333f67b8cfa99ef4038685b))

## [0.5.0](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.16...v0.5.0) (2023-07-18)


### Features

* add npm publisher ([1a747f7](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/1a747f7cbd2428ecf70172976bcc2def739df623))
* add Nuget publisher ([2f4ede9](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/2f4ede9ccce1b1ef2269c38e7692e2f6e12ddd20))

## [0.4.16](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.15...v0.4.16) (2023-07-12)


### Bug Fixes

* handle v in hoppr version, add unit tests ([e6b2653](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/e6b2653ee63d0bbabce7c38f939a6cf3eb322182))

## [0.4.15](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.14...v0.4.15) (2023-07-07)


### Bug Fixes

* **deps:** update dependency hoppr to v1.8.7 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([19d2316](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/19d23163a3c69a5bb3cf84160740c662fa168d29))

## [0.4.14](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.13...v0.4.14) (2023-07-06)


### Bug Fixes

* check Hoppr version in Docker publisher to determine pathing format ([1397af4](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/1397af41eaa1f5699849ce956ea8f44c4e6b3c6a))
* Gather hoppr version ([3fc5423](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/3fc5423e4b79094dd5698afa14c5c0aace440f67))
* update version check to use semver library ([c0e5212](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/c0e521210a82a3e67670471fef8b04c1ffb1aa9e))

## [0.4.13](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.12...v0.4.13) (2023-05-16)


### Bug Fixes

* removing npm config which is not used in the project ([0160bdd](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/0160bdda6c16bde96fc4d3e3c75960e8f16ce680))

## [0.4.12](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.11...v0.4.12) (2023-05-03)


### Bug Fixes

* **deps:** update dependency lunr-languages to v1.12.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([dd3325c](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/dd3325c6a92ec09ac5dcbf637f49bbdbb2d4dee3))

## [0.4.11](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.10...v0.4.11) (2023-05-02)


### Bug Fixes

* **deps:** update dependency @mui/material to v5.12.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([6c42058](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/6c4205873c4e1b78543620a51a121226ee7ed2fe))

## [0.4.10](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.9...v0.4.10) (2023-04-27)


### Bug Fixes

* **deps:** update dependency rxjs to v7.8.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([6833c26](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/6833c261df5fff9667a8f7515311da54c9279b29))

## [0.4.9](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.8...v0.4.9) (2023-04-26)


### Bug Fixes

* **deps:** update dependency lunr-languages to v1.11.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([dbd1147](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/dbd114778a9b32a5ac4a2b014d4e90f8dff0ec94))

## [0.4.8](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.7...v0.4.8) (2023-04-26)


### Bug Fixes

* **deps:** update dependency hoppr to v1.8.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([0d2c9a7](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/0d2c9a77b153962d21799c46f5c6d86c17d5e2fc))

## [0.4.7](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.6...v0.4.7) (2023-04-25)


### Bug Fixes

* **deps:** update dependency @mui/material to v5.12.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([21daf5f](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/21daf5f1234bc0f11092988507171830ca7bd703))

## [0.4.6](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.5...v0.4.6) (2023-04-24)


### Bug Fixes

* **deps:** update dependency hoppr to v1.8.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9c9a4e4](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/9c9a4e474ca334fef5bea43c7aa6fc1727d7510f))

## [0.4.5](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.4...v0.4.5) (2023-04-18)


### Bug Fixes

* **deps:** update dependency hoppr to v1.8.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ac1b0e6](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/ac1b0e6b2ce64581f5e149cb2ea51a7bb9eafa82))

## [0.4.4](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.3...v0.4.4) (2023-04-17)


### Bug Fixes

* **deps:** update dependency @mui/material to v5.12.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e034f67](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/e034f67c36a096bd1c97d48997f42cc4390c641b))

## [0.4.3](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.2...v0.4.3) (2023-04-13)


### Bug Fixes

* **deps:** update dependency hoppr to v1.8.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([51a9c35](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/51a9c35cb351073ebc0e81d79e07d2c89342bee2))

## [0.4.2](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.1...v0.4.2) (2023-04-12)


### Bug Fixes

* Correct log name ([7f3aee5](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/7f3aee52647103effee334cb6e29c1e374515791))
* Correct semantic-release to match core hoppr ([85b4c77](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/85b4c776bbff22756dc459f4a891befb6fda25c5))
* Correct test image ([708215b](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/708215b9e9157edce438591a75315fa0e60c4c22))
* **deps:** replace dependency @material-ui/core with @mui/material ^5.0.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([b32c9a4](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/b32c9a46516dbf4646a4877ad62389a042f822a3))
* **deps:** update dependency @mui/material to v5.11.14 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([4a465fb](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/4a465fb0788cae699015c29e6090c7e6e9974c35))
* **deps:** update dependency @mui/material to v5.11.15 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([15f031c](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/15f031c7d7212a392617e720f458bd32d2a6f6c4))
* **deps:** update dependency @mui/material to v5.11.16 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([688acf8](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/688acf8bff3a8b3e53b56f1f127d7d68d8832bf4))
* **deps:** update dependency @mui/material to v5.12.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([4ca6ea4](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/4ca6ea46c8d1a8da33009a0a42e2c9f6bb83208c))
* **deps:** update dependency iframe-worker to v1.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([cb2b47f](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/cb2b47f9a06bcba38a04d0392273381dcdaac604))
* **deps:** update dependency lunr-languages to v1.10.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([df773c2](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/df773c2f3d702148902e129f177b55e9dd2e9321))
* **deps:** update dependency rxjs to v7.8.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([c1c5bcd](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/c1c5bcdf20012d981fda9ba33814ace4f9dacf6f))
* Increase timeout for maven ([6443098](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/6443098159474493b303653ae4d874777a3bb1ef))
* Keep logs always ([459d2b5](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/459d2b5acfd6619591e60fe2949fab974768870f))
* missing timeout  for `requests` methods ([82d5786](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/82d578652abb197ef224b5c4ea9e8c5a7fb3d1e5))
* pylint duplicate-code error ([e3bb8c3](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/e3bb8c3dd6ef9bc039cf71281aa4791036c66d2e))
* Reference hopctl image ([213b40d](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/213b40d92e2fdf3b982e9b240641d09ed90a7656))
* Set entrypoint override ([073055b](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/073055b3f580b998d77676eec7f6d6a8c6f37fcf))
* Setup all tools ([c0f3bdd](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/c0f3bdd8b6eda5d04d8b9d2ecffcb78cbf8e4090))

## [0.4.2](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.1...v0.4.2) (2023-04-12)


### Bug Fixes

* Correct log name ([7f3aee5](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/7f3aee52647103effee334cb6e29c1e374515791))
* Correct semantic-release to match core hoppr ([85b4c77](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/85b4c776bbff22756dc459f4a891befb6fda25c5))
* Correct test image ([708215b](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/708215b9e9157edce438591a75315fa0e60c4c22))
* **deps:** replace dependency @material-ui/core with @mui/material ^5.0.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([b32c9a4](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/b32c9a46516dbf4646a4877ad62389a042f822a3))
* **deps:** update dependency @mui/material to v5.11.14 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([4a465fb](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/4a465fb0788cae699015c29e6090c7e6e9974c35))
* **deps:** update dependency @mui/material to v5.11.15 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([15f031c](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/15f031c7d7212a392617e720f458bd32d2a6f6c4))
* **deps:** update dependency @mui/material to v5.11.16 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([688acf8](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/688acf8bff3a8b3e53b56f1f127d7d68d8832bf4))
* **deps:** update dependency @mui/material to v5.12.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([4ca6ea4](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/4ca6ea46c8d1a8da33009a0a42e2c9f6bb83208c))
* **deps:** update dependency iframe-worker to v1.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([cb2b47f](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/cb2b47f9a06bcba38a04d0392273381dcdaac604))
* **deps:** update dependency lunr-languages to v1.10.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([df773c2](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/df773c2f3d702148902e129f177b55e9dd2e9321))
* **deps:** update dependency rxjs to v7.8.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([c1c5bcd](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/c1c5bcdf20012d981fda9ba33814ace4f9dacf6f))
* Increase timeout for maven ([6443098](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/6443098159474493b303653ae4d874777a3bb1ef))
* Keep logs always ([459d2b5](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/459d2b5acfd6619591e60fe2949fab974768870f))
* missing timeout  for `requests` methods ([82d5786](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/82d578652abb197ef224b5c4ea9e8c5a7fb3d1e5))
* pylint duplicate-code error ([e3bb8c3](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/e3bb8c3dd6ef9bc039cf71281aa4791036c66d2e))
* Reference hopctl image ([213b40d](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/213b40d92e2fdf3b982e9b240641d09ed90a7656))
* Set entrypoint override ([073055b](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/073055b3f580b998d77676eec7f6d6a8c6f37fcf))
* Setup all tools ([c0f3bdd](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/c0f3bdd8b6eda5d04d8b9d2ecffcb78cbf8e4090))

## [0.4.2](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.1...v0.4.2) (2023-04-12)


### Bug Fixes

* Correct test image ([708215b](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/708215b9e9157edce438591a75315fa0e60c4c22))
* **deps:** replace dependency @material-ui/core with @mui/material ^5.0.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([b32c9a4](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/b32c9a46516dbf4646a4877ad62389a042f822a3))
* **deps:** update dependency @mui/material to v5.11.14 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([4a465fb](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/4a465fb0788cae699015c29e6090c7e6e9974c35))
* **deps:** update dependency @mui/material to v5.11.15 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([15f031c](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/15f031c7d7212a392617e720f458bd32d2a6f6c4))
* **deps:** update dependency @mui/material to v5.11.16 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([688acf8](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/688acf8bff3a8b3e53b56f1f127d7d68d8832bf4))
* **deps:** update dependency @mui/material to v5.12.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([4ca6ea4](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/4ca6ea46c8d1a8da33009a0a42e2c9f6bb83208c))
* **deps:** update dependency iframe-worker to v1.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([cb2b47f](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/cb2b47f9a06bcba38a04d0392273381dcdaac604))
* **deps:** update dependency lunr-languages to v1.10.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([df773c2](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/df773c2f3d702148902e129f177b55e9dd2e9321))
* **deps:** update dependency rxjs to v7.8.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([c1c5bcd](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/c1c5bcdf20012d981fda9ba33814ace4f9dacf6f))
* missing timeout  for `requests` methods ([82d5786](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/82d578652abb197ef224b5c4ea9e8c5a7fb3d1e5))
* pylint duplicate-code error ([e3bb8c3](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/e3bb8c3dd6ef9bc039cf71281aa4791036c66d2e))
* Reference hopctl image ([213b40d](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/213b40d92e2fdf3b982e9b240641d09ed90a7656))
* Set entrypoint override ([073055b](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/073055b3f580b998d77676eec7f6d6a8c6f37fcf))
* Setup all tools ([c0f3bdd](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/c0f3bdd8b6eda5d04d8b9d2ecffcb78cbf8e4090))

## [0.4.1](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.4.0...v0.4.1) (2023-01-26)


### Bug Fixes

* **deps:** update dependency iframe-worker to v1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([2b17a30](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/2b17a30c69927c52115f0b85536c98690591a201))
* **deps:** update dependency unfetch to v5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([81d9379](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/81d9379e908c55bacc2c549440a4347e314de130))

## [0.4.0](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/compare/v0.3.2...v0.4.0) (2023-01-23)


### Features

* add apt publisher ([22690b9](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/22690b93ac39d8677a6e8272d9fe899c9fa3c72b))


### Bug Fixes

* add delivered_sbom to test contexts ([64996be](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/64996be459ce4db50480f0ce2aea11a092416928))
* change bom paths in integration tests to be relative to manifest file ([f0a3a21](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/f0a3a21faecc731076cde254a02910a4ffaecf83))
* create local semantic-release.yml file ([25806ca](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/25806ca4596da587e64ae2936e3bbbeb6f9ea7f2))
* docker_publisher issue ([f6bd05c](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/f6bd05c697405cc0bd3ebe974305bc5c642f56a8))
* move location of hoppr project in gitlab ([bb7d32c](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/bb7d32cbcd4499fb368c429adeadfb6004374740))
* pull semrel from hoppr dev, not main ([9e748e3](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/9e748e3061e7233537858f110a0995300fbec0da))
* unit test env var issue ([5c4bb7d](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/5c4bb7d7c707fa0adf76521849b7c5203a76fbd7))
* use underscore instead of dash on maven file names ([4ff69f8](https://gitlab.com/hoppr/plugins/hoppr-nexus-bundler/commit/4ff69f8bc912c7a52512182318d7fb0357851bf9))

## [0.3.2](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/compare/v0.3.1...v0.3.2) (2022-11-01)


### Bug Fixes

* add back pre stage for semantic versioning ([d059586](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/d059586d99312458265e6938f7b6eeae9c1b52cb))
* update transfer config files ([a29182d](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/a29182d5ab5b6e69a401b4a68bbddec448f2123f))

## [0.3.1](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/compare/v0.3.0...v0.3.1) (2022-09-20)


### Bug Fixes

* allow override of skopeo command in config ([d523c81](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/d523c812eb444af70b695997c63f83180e904002))
* update for hoppr log file ([a610fd7](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/a610fd7e17b0ffa5e896679cdfb157fb6ccc013d))

## [0.3.0](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/compare/v0.2.1...v0.3.0) (2022-09-13)


### Features

* add integration tests, first try ([b05fa44](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/b05fa444203a5d688cd60f1f832af0a5aef7c07d))


### Bug Fixes

* add curl ([1d67a57](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/1d67a57e5b7d30a8d98ce691e698cee821145f9f))
* add debug ([138b18f](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/138b18f76851d3a7945bfad066a3d31e52384432))
* add docker repo for yum ([9e39d70](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/9e39d70f7ec4c191ed7471962f160cb3cea25345))
* add DOCKER variables ([9aa9774](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/9aa97747dfdd05bcb9bbe2da485df938c8cdde3c))
* add DOCKER_TLS_CERTDIR variables ([846c1c0](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/846c1c00e8c267209ea4c908763bcec314af8e17))
* add docker-builder tag ([747e296](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/747e296bceabb6aee5e57217aea597431d86d57b))
* add name ([4bba6bc](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/4bba6bcc7442a6d40dc26ab789fbcea638d2f38b))
* another attempt at dind ([26a7579](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/26a7579c6e8d244157e409e6ef9f5ee8a2259ba2))
* another backslash ([f967504](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/f96750438fc47aef0ab1867910f61d6312e6cef6))
* another backslash ([e9ecc46](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/e9ecc4601986702a0a28609eeb33d99d7da2738f))
* back to using local test image ([3c2f7bf](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/3c2f7bf9901fe7a389a4e5049f378e7d1a3dab8b))
* build custom rocky docker int image ([67e05c0](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/67e05c0c2913369990545da8cbaca8eedc75937a))
* chmod ([713cb73](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/713cb737258d9ffa5e0454f12de53a3237cd99b8))
* chop docker sboms in half ([2e1283b](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/2e1283be074d5166805f48383fc890ec2d03f54e))
* clean-up ([957d4b7](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/957d4b7f24cc8f281044145410b91968882128be))
* debug ([ec2c73f](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/ec2c73fba5b77f0852e3a753b5ad7f5084e7df2a))
* don't need build-test-images ([598b31a](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/598b31a4ada78577fb716305c81beadc102b8721))
* empty test syntax ([4ca7b46](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/4ca7b46ddfd16a7b58b139d164eb433115475775))
* escape $'s ([f1be0af](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/f1be0af168f1616a7483bc80f9c15f98f85e599e))
* export NEXUS variables in script ([08cb2fe](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/08cb2fea71acdf14b62dbea64460dac0d4bce2b7))
* get dist image for plugin ([64cf3c2](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/64cf3c23e94873895da30f3957370ebede7cdecc))
* hide unneeded stderr, add TEST_ID ([13cc5ce](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/13cc5ce74c05f0c13d45f9bb59a41afb6358bcee))
* indenting ([8fe12ae](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/8fe12aedd1c79b5fb697132e5f59a17fc555f2ae))
* install docker cli ([c602c8f](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/c602c8f3a15f22f676228314a6ca35d57e09e498))
* just set tls flag ([2b47fa0](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/2b47fa0ca251ea437066bb0e9d9dbc19724b4e04))
* more backslashes ([85b3709](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/85b3709297b9dfd3a9504d1adde4d8d988c8350d))
* more cleanup ([4480c5c](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/4480c5cde88ba3307b65c8270c9fd73892885fba))
* more docker installs, re-install skopeo ([144fe91](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/144fe91957dada9fcfda7757a7d937a4479c40db))
* move nexus checks to _run_test ([9bd4ff1](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/9bd4ff1296857eef0f0e34eb1dba59e63cc5a40d))
* name ([8e4bc0c](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/8e4bc0ccef296f7478514370704c81c8e05c6ced))
* Only run Integration tests in main branch ([4816a83](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/4816a83ed899689b390f4ecfe4370515a9bce533))
* pass in TEST_IMAGE ([217a08b](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/217a08b3a51b36d4b23f5395e6128e9148b4358f))
* remove -i ([084c4d2](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/084c4d219740b782665a7d1a8d6c069e629fe610))
* remove quotes ([9ecb7ac](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/9ecb7ac98cae232b88a55eef694d9eb2f335fe9f))
* run all tests, another backslash ([2d2f02f](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/2d2f02fee932fff8768a1e48c39bba35d86cce2d))
* single line ([68c83df](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/68c83dfc2bb8ca2e9350992a664ed30b2e578f76))
* specify dind version ([a65f2ad](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/a65f2ad8022d47291422c52bfc9c8118d9e97a04))
* start docker daemon ([0bf75ec](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/0bf75ecfa586fad21a4cce1c549e0a5d7ef3337f))
* starting docker ([cc25c1a](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/cc25c1a5fc91cff0dbb67c177a8ad927fbf5b259))
* syntax ([b1ee915](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/b1ee915685fd6c760f60c2a675418bbd85bd2690))
* syntax ([b4fbedf](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/b4fbedf7cab9515feb453eb5de3bf1269dff1bca))
* syntax ([939698f](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/939698f6154e2dfc84ac7a63e8381577c2763b40))
* try /bin/bash ([29eb9a9](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/29eb9a9bd75efa38bd02d7e8641608273e70e674))
* try int_test image ([d353028](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/d35302891f4d13bed6a9822fe0bccdfaadf79805))
* try removing network=host ([735c88b](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/735c88b89c7a1b145637791f391e487af988207c))
* try setting overlay docker driver ([870b680](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/870b680dd92e9e42f48dda9fe4a53515dc8d5f63))
* try to get PW before fully up ([01b7754](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/01b77543c3193e12e4ecb154d8321f14908cb71d))
* try to make nexus bigger ([7000bdb](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/7000bdbf5fc1e172fb2e13dd0e544bed74bfa6cb))
* trying dind service ([c2232c3](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/c2232c37ff195d21e48a98b3f70b52719406663f))
* trying docker tag, no dockerd ([ed9fa5f](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/ed9fa5f70afe8457c8b6827ba070b3ef525b1b3b))
* trying running test image in docker ([3ac363b](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/3ac363b37aaf1fa1fab58b4b9411bca7a1ec972b))
* trying sevice w/docker tag ([afd53bf](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/afd53bf2fac5e9292102baad270615dde81dab47))
* trying simple dind example ([f4c4a1b](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/f4c4a1bfe05a87ab048fbce08a0b946c940c9feb))
* trying to get NEXUS_HOST passed in ([4cdc344](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/4cdc344a7470e70ffb01cf4a0b21d645b3273388))
* wait for new nexus to come up ([56d3340](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/56d3340d7dae737d06c89dbeae8d7f7ae985490c))
* wait for nexus by checking pw file ([59a05f4](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/59a05f41c81d0939ac826d82379888ac6ed06197))
* whl file name ([fdcc225](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/fdcc2258b2c018c7a5bf911325a3ae7cd7f1775a))

## [0.2.1](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/compare/v0.2.0...v0.2.1) (2022-09-01)


### Bug Fixes

* added additional purl types to git publisher ([142bce2](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/142bce28d4db4b3bb80740eb061a1a1674e7bf81))

## [0.2.0](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/compare/v0.1.2...v0.2.0) (2022-08-11)


### Features

* Initial yum publisher ([9edb25b](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/9edb25bb88426b7ed85386d03f38bab817e0fb29))


### Bug Fixes

* black formatting ([382debd](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/382debd4eecbda7a04e067c51f292f6d393ff30b))
* change log message per code review comment ([414f8a6](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/414f8a6db32fb1928f31441604e5cd63ea401c46))
* merge from main, lint issues ([37a68bb](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/37a68bbaa902f751215f44d48de6182427823747))
* more merge issues ([ec846e2](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/ec846e28b5a2c1a9bd58822a10a2a8addb6dc745))
* poetry.lock ([73eed9b](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/73eed9b97d6e1fd938d0d6a0a4d7620faec061c7))
* Yum publisher updates, transition to external cyclonedx models ([f9365a2](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/f9365a29b403e72a910f9b06f150766471f8fad8))

## [0.1.2](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/compare/v0.1.1...v0.1.2) (2022-08-04)


### Bug Fixes

* black format ([3835e67](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/3835e677aa643b2e8e45553683e9dc5fc0655eb0))
* black formatting ([0970653](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/0970653350be91c816eb0c7b47dd2c43f73e011d))
* linter errors ([ffea10c](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/ffea10c52555dbf062094afbd6c90fe18a954fa1))

## [0.1.1](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/compare/v0.1.0...v0.1.1) (2022-08-02)


### Bug Fixes

* renamed folder to name package name. ([6252c28](https://gitlab.com/lmco/hoppr/plugins/hoppr-nexus-bundler/commit/6252c28293fd5f47053fb7f2cc68c3ee61aa55fd))

## [1.1.2](https://gitlab.com/lmco/hoppr/plugins/nexus-bundler/compare/v1.1.1...v1.1.2) (2022-08-02)


### Features

* git_publisher complete ([743d8b3](https://gitlab.com/lmco/hoppr/plugins/nexus-bundler/commit/743d8b34fa0d6216d02526ab015dcca725bfd879))


### Bug Fixes

* black formatting ([4b419e2](https://gitlab.com/lmco/hoppr/plugins/nexus-bundler/commit/4b419e2186585a28d587ea88c3f6982aeaa1a865))
* black formatting error ([df18ffb](https://gitlab.com/lmco/hoppr/plugins/nexus-bundler/commit/df18ffbcec6833460169809df8be8fd8efc28fad))
* git-publisher changes for bundler update ([58c5eb0](https://gitlab.com/lmco/hoppr/plugins/nexus-bundler/commit/58c5eb0a85386fc6df2fc773c71bb338fea3785b))

## [1.1.1](https://gitlab.com/lmco/hoppr/plugins/nexus-bundler/compare/v1.1.0...v1.1.1) (2022-08-02)


### Bug Fixes

* Add LICENSE ([28795f5](https://gitlab.com/lmco/hoppr/plugins/nexus-bundler/commit/28795f5befb757d5020829dfd3cc4aafe4568fb5))

## [1.1.0](https://gitlab.com/lmco/hoppr/plugins/nexus-bundler/compare/v1.0.0...v1.1.0) (2022-07-28)


### Features

* initial multi-processing ([c91334c](https://gitlab.com/lmco/hoppr/plugins/nexus-bundler/commit/c91334cd26ace30552a678ac87a08a7dfccc1393))


### Bug Fixes

* linting fixes ([8059aeb](https://gitlab.com/lmco/hoppr/plugins/nexus-bundler/commit/8059aeb48a29677a73b3e17ca69a33232648384d))
* Use MemoryLogger, create publisher per instance (rather than per purl type) ([3696094](https://gitlab.com/lmco/hoppr/plugins/nexus-bundler/commit/3696094d2d9bba1f8698faa8d66f018bf1ed2eb1))

## 1.0.0 (2022-07-21)


### Features

* initial cut at publisher classes ([a354734](https://gitlab.com/lmco/hoppr/plugins/nexus_bundler/commit/a35473490d3998674c17fc9a86e19af91b291468))
* initial groundwork checkin ([6126665](https://gitlab.com/lmco/hoppr/plugins/nexus_bundler/commit/6126665ce15a605dd5b6d407361d860abdc2358d))
* Populate nexus instance, initial driver code ([4aa5d52](https://gitlab.com/lmco/hoppr/plugins/nexus_bundler/commit/4aa5d5253123ebadafcee8964995202577587ee8))


### Bug Fixes

* add black, pylint, .releaserx.yml ([ddee82f](https://gitlab.com/lmco/hoppr/plugins/nexus_bundler/commit/ddee82f55bc9f6cc3080200a1fe1850551d66a61))
* add port-forwarding example ([b39c843](https://gitlab.com/lmco/hoppr/plugins/nexus_bundler/commit/b39c843c42cf015e5bf486dc7d5bf1230afa05f1))
* add pytest ([f3f022a](https://gitlab.com/lmco/hoppr/plugins/nexus_bundler/commit/f3f022af4cb0a6cc9d8f35a35cb927e3fdbccac0))
* added mypy, package.json ([9383489](https://gitlab.com/lmco/hoppr/plugins/nexus_bundler/commit/938348979c75cd14bd198be5c1eca53c77e84bb9))
* Added unit tests, minor fixes ([f47f43b](https://gitlab.com/lmco/hoppr/plugins/nexus_bundler/commit/f47f43b7235552ce50b74d7ae0e422047f8e7c44))
* Code review comments ([ebd5818](https://gitlab.com/lmco/hoppr/plugins/nexus_bundler/commit/ebd5818d3e5761d2c6a9775645a0c4908acdffc8))
* typo ([fb71fb4](https://gitlab.com/lmco/hoppr/plugins/nexus_bundler/commit/fb71fb4e8bebf556e155a5dce186f5f146b763fb))
* update .releaserc.yml ([0245c01](https://gitlab.com/lmco/hoppr/plugins/nexus_bundler/commit/0245c014e9eb83b60d462b885686a60316ef0b65))
* Update README.md ([9944b7d](https://gitlab.com/lmco/hoppr/plugins/nexus_bundler/commit/9944b7de24c5ad79b3b819b94799adc21f1747df))
